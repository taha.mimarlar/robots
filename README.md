# Robots

The creator of the site tries to hide something. Can you find it?

**Solution**
Since the name of the challenge is robots the user needs to check robots.txt. Robots.txt will lead the user to the hidden information.
